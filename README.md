## Sobre

Repositório de documentação do grupo 2.3 no projeto Fintech. Este espaço traz todos os documentos produzidos ao longo do processo de desenvolvimento do projeto, desde documentos técnicos até atas de reuniões semanais.

[GitLab Pages](https://documentacao-fintech-grupo3-c2874e9ddeea1914ab26d1ca6ca0d28f58e.gitlab.io/)


## Ambientes

- 

## Integrantes

### 2024-1

| Nome | Matricula | Gitlab | Email |
|-----------|------|--------|------|
| João Vitor Ferreira Alves | 160127912 |  vitorAlves7 | 160127912@aluno.unb.br |
| Kayro César Silva Machado | 170107426 |  kayrocesar | kayro.cesar.kc@gmail.com |
| Lude Yuri de Castro Ribeiro | 150137770 | luderibeiro | 150137770@aluno.unb.br |
| Paulo Gonçalves Lima | 170122549 |  PauloGoncalvesLima | paulogoncalves436@gmail.com |


