# Plano de qualidade

## Introdução

A qualidade é essencial para garantir a confiabilidade e o desempenho dos serviços. Este plano delineia as estratégias e ferramentas que serão empregadas para assegurar que os serviços atendam aos mais altos padrões de qualidade, identificando problemas, garantindo a funcionalidade correta e promovendo a integridade do software.

## Ferramentas

| Ferramenta  | Descrição                       |
| ----------- | ------------------------------- |
| Sonar Cloud | Ferramenta de análise de código |
| Jest        | Framework de teste JavaScript   |
| Jasmine     | Framework de teste JavaScript   |
| Pytest      | Framework de teste para Python  |


## Sonar Cloud

- O SonarCloud é uma ferramenta de análise de código estática que fornece insights detalhados sobre a qualidade do código, identificando problemas, vulnerabilidades e áreas de melhoria. Integraremos o SonarCloud aos nossos serviços para garantir que o código seja escrito de acordo com os mais altos padrões de qualidade e segurança.

### Detalhes do uso do SonarCloud:

- Análise de Código Contínua: Integraremos o SonarCloud aos nossos fluxos de trabalho de desenvolvimento, garantindo que cada alteração de código seja automaticamente analisada quanto à qualidade e à segurança.
Identificação de Vulnerabilidades e Bugs: O SonarCloud identificará automaticamente vulnerabilidades de segurança, bugs e problemas de qualidade no código dos nossos serviços, permitindo sua correção rápida e eficiente.

- Acompanhamento de Métricas de Código: Acompanharemos de perto as métricas fornecidas pelo SonarCloud, incluindo a cobertura de código, a complexidade e a duplicação de código, para garantir que nosso código seja limpo, eficiente e seguro.

- Integração com Fluxos de Trabalho: Integraremos o SonarCloud com nossos fluxos de trabalho de desenvolvimento e integração contínua, permitindo a revisão e a correção de problemas de qualidade durante todo o ciclo de desenvolvimento dos nossos serviços.


## Jest

- O Jest, embora seja mais comumente associado ao JavaScript, também pode ser usado para testar código escrito em TypeScript, uma linguagem comum em muitos serviços. Planejamos aproveitar as capacidades do Jest para testar componentes front-end e back-end de nossos serviços, garantindo a funcionalidade correta e a integridade do software.

### Detalhes do uso do Jest:

- Testes de Componentes: Utilizaremos o Jest para realizar testes em componentes front-end e back-end, garantindo que eles funcionem conforme o esperado em diferentes cenários.

- Testes Automatizados: Implementaremos uma suíte de testes automatizados que será executada sempre que houver alterações no código, garantindo a detecção precoce de problemas.

- Cobertura de Testes: Acompanharemos a cobertura de testes para garantir que todas as partes do código sejam testadas adequadamente, identificando áreas que precisam de mais atenção.


## Jasmine

O Jasmine é outro framework de teste JavaScript que oferece uma sintaxe limpa e uma ampla gama de recursos para escrever testes claros e concisos. Planejamos usar o Jasmine para testar componentes específicos de nossos serviços JavaScript, complementando as capacidades do Jest e garantindo uma cobertura abrangente de testes.

### Detalhes do uso do Jasmine

- Testes de Componentes Específicos: Utilizaremos o Jasmine para testar componentes específicos de nossos serviços JavaScript, aproveitando sua sintaxe limpa e sua facilidade de uso.

- Integração com Outras Ferramentas: Integraremos o Jasmine com nossas ferramentas de desenvolvimento e integração contínua, garantindo que os testes sejam executados automaticamente em cada alteração de código.

- Monitoramento da Cobertura de Testes: Acompanharemos de perto a cobertura de testes fornecida pelo Jasmine, garantindo que todas as partes do código sejam adequadamente testadas.

## Pytest

- O Pytest é um framework de teste para Python que oferece uma estrutura simples e poderosa para escrever e executar testes. Planejamos utilizar o Pytest para realizar testes unitários e de integração em nossos serviços escritos em Python, garantindo a qualidade e a funcionalidade correta do código.

### Detalhes do uso do Pytest:

- Testes Unitários: Criaremos testes unitários para avaliar o comportamento de unidades individuais de código, como funções e métodos, em nossos serviços Python.

- Testes de Integração: Realizaremos testes de integração para avaliar a interação e a comunicação entre diferentes componentes de nossos serviços, garantindo o correto funcionamento do sistema como um todo.

- Cobertura de Testes: Monitoraremos a cobertura de testes para garantir que a maioria do código seja testada, identificando áreas que requerem atenção adicional.

- Testes Automatizados: Configuraremos uma suíte de testes automatizados para ser executada continuamente, garantindo que as alterações no código não introduzam regressões.

## Histórico de versão
| Data | Versão | Descrição | Autor(es) |
| ---- | ---- | ---- | ---- |
| 7/05/2024 | 1.0 | Criação do Documento | Paulo Gonçalves Lima |

## Referências

> SONNAR CLOUD, <https://www.sonarsource.com/products/sonarcloud/>.
> JEST, <https://jestjs.io/pt-BR/>.
> PYTEST, <https://docs.pytest.org/>.
> JASMINE, <https://jasmine.github.io/>.
