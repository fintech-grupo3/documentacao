##  Estrutura Analítica do Projeto (EAP)

<p align="justify"> 
No contexto do gerenciamento de projetos, é importante reconhecer que alterações frequentes no escopo podem causar perturbações e desordem. Para lidar com isso de maneira eficaz, a criação de uma Estrutura Analítica do Projeto (EAP) pode ser essencial, proporcionando uma gestão mais precisa e organizada.
</p>

<p align="justify"> 

A Estrutura Analítica do Projeto (EAP) é um diagrama visual que organiza o escopo do projeto de maneira hierárquica, dividindo-o em partes menores para facilitar o gerenciamento das entregas.
</p>


![Disponibilidade](../assets/plan/eap.png)
