# Quadro de Disponibilidade



## Introdução 

O quadro de disponibilidade é uma ferramenta amplamente utilizada que permite visualizar a disponibilidade dos membros da equipe em um determinado projeto.



## Contexto
No contexto do grupo, visando uma melhor administração e agendamento de possíveis reuniões, foi construído um quadro de disponibilidade.


![Disponibilidade](../assets/Schedules/canvas_disp.png)

## Histórico de versão
| Data | Versão | Descrição | Autor(es) |
| ---- | ---- | ---- | ---- |
| 17/04/2024 | 1.0 | Criação do Documento | Kayro César| 