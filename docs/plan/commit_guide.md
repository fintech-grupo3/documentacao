## Guia de commit

Esse guia tem o objetivo de definir um padrão para os commits no projeto.

Padão de commit

### **prefixo(sufixo): descrição**

**Exemplo**:

```markdown
docs(dag-notificacao-proposta): documentação inicial da dag de envio de mensagem de novas propostas.
```

### Prefixo

- **feat**: Utilizado para novas funcionalidades ou adições ao código. Feat de features.
- **fix**: Usado para correções de bugs.
- **chore**: Geralmente associado a tarefas de manutenção, ajustes de configuração ou outras atividades não relacionadas a funcionalidades ou bugs.
- **docs**: Reservado para alterações na documentação.
- **refactor**: Utilizado quando há refatoração de código sem alterar comportamento externo.
- **test**: Associado a adições ou modificações nos testes.
- **build**: Relacionado a mudanças no sistema de build ou dependências.
- **ci**: Envolvendo ajustes ou melhorias em configurações de integração contínua.
- **perf**: Usado para melhorias de desempenho.
- **revert**: Utilizado para reverter uma alteração anterior.

### Sufixo

No sufixo, deve estar em parentese aonde que está sendo feita a mudança, seguido de dois pontos e sua mensagem.

## Revisão e qualidade do código

Para manter a qualidade do código, adotamos uma série de medidas automatizadas e de padrão de código.

### Commit em grupo
```
docs(dag-notificacao-proposta): documentação inicial da dag de envio de mensagem de novas propostas.
Co-authored-by: Paulo Gonçalves 170122549@aluno.unb.br
```

## Histórico de versão
| Data | Versão | Descrição | Autor(es) |
| ---- | ---- | ---- | ---- |
| 09/04/2024 | 1.0 | Criação do Documento | Paulo Gonçalves |