# Plano de Gerenciamento de Riscos

## Introdução


O objetivo do Plano de Gestão de Riscos é reconhecer, examinar e preparar medidas para os potenciais riscos que podem surgir durante o projeto. Após o planejamento, a administração dos riscos torna-se viável.

## Estrutura Analítica de Riscos (EAR)

A estrutura analítica de riscos tem a função de agrupar as potenciais fontes de riscos, o que simplifica a identificação e o tratamento de riscos no projeto, agilizando o processo de mitigação. Seu objetivo é destacar as principais categorias de risco para um determinado tipo de projeto, visando a especificidade, pois isso resulta em economia de tempo na identificação. Os riscos podem ser categorizados conforme:

* **Técnico**: São riscos associados à tecnologia, requisitos e qualidade.
* **Externo**: São os riscos relativos ao cliente, mercado ou ambiente.
* **Organizacional**: São relacionados à priorização e recursos do projeto.
* **Gerência**: São relativos à estimativa, planejamento, controle e comunicação.

## Diagrama da Estrutura Analítica de Riscos





![EAR](../assets/plan/ear.png)
<center>


Figura 1 – Diagrama EAR

</center>



## Análise Quantitativa e Qualitativa de Riscos

Na fase de Análise Quantitativa de Riscos, são realizadas avaliações numéricas da probabilidade, certeza e impacto dos riscos. Já na etapa de Análise Qualitativa de Risco, identificam-se quais riscos requerem maior atenção ou ações adicionais, considerando a relação entre a probabilidade de ocorrência e o impacto. As Tabelas 1, 2, 3 e 4 apresentam como esses critérios são determinados.

<center>

| Probabilidade | Certeza     | Peso |
| ------------- | ----------- | ---- |
| Nulo          | 0%          | 0    |
| Muito baixa   | ]0%, 20%[   | 1    |
| Baixa         | [20%, 40%[  | 2    |
| Média         | [40%, 60%[  | 3    |
| Alta          | [60%, 80%[  | 4    |
| Muito alta    | [80%, 100%] | 5    |

Tabela 1 – Identificação de peso da Probabilidade

</center>

<center>

| Impacto     | Descrição                       | Peso |
| ----------- | ------------------------------- | ---- |
| Nulo        | Nulo                            | 0    |
| Muito Baixo | Pouco Expressivo                | 1    |
| Baixo       | Pouco Impacto                   | 2    |
| Médio       | Impacto Médio                   | 3    |
| Alto        | Grande Impacto                  | 4    |
| Muito Alto  | Impede a continuação do projeto | 5    |

Tabela 2 –Identificação de peso do Impacto

</center>

<center>

| Impacto X Probabilidade | Nulo | Muito Baixo | Baixo | Médio | Alto | Muito Alto |
| ----------------------- | ---- | ----------- | ----- | ----- | ---- | ---------- |
| Nulo                    | 0    | 0           | 0     | 0     | 0    | 0          |
| Muito Baixo             | 0    | 1           | 2     | 3     | 4    | 5          |
| Baixo                   | 0    | 2           | 4     | 6     | 8    | 10         |
| Médio                   | 0    | 3           | 6     | 9     | 12   | 15         |
| Alto                    | 0    | 4           | 8     | 12    | 16   | 20         |
| Muito Alto              | 0    | 5           | 10    | 15    | 20   | 25         |

Tabela 3 – Identificação de Grau de Risco

</center>

<center>

| Grau de Risco  | Descrição |
| -------------- | --------- |
| 0              | Nulo      |
| 0 < Risco <= 5 | Baixo     |
| 5 < Risco < 15 | Médio     |
| 15 <= Risco    | Elevado   |

Tabela 4 – Classificação do Grau de Risco

</center>

## Identificação dos Riscos


No processo de identificação de riscos foi utilizada a técnica de comparação análoga, que se baseia em experiências anteriores e similares para facilitar a concepção e identificação de riscos comuns em projetos similares.


## Riscos Levantados para o contexto do projeto



<center>


| ID Risco | Risco                                                        | Consequência                                                                                         | Probabilidade | Impacto | Grau de Risco | Estratégias de Prevenção                                           | Estratégias de Mitigação                                                                 |
| -------- | ------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------- | ------------- | ------- | ------------- | ----------------------------------------------------------------- | ----------------------------------------------------------------------------------------- |
| 1        | Desistência de membros do grupo ou abandono da disciplina    | Atraso ou entrega parcial projeto                                                             | 2             | 4       | 8             | Manter uma comunicação eficaz, planejamento e organização ao longo do semestre  | Reatribuição de responsabilidades entre os membros restantes                          |
| 2        | Cronograma mal definido                                      | Redução do tempo disponível para realização das tarefas                                               | 2             | 4       | 8             | Atentar-se aos prazos da disciplina e ao tempo necessário para cada tarefa | Alocar mais recursos para as tarefas em atraso                                          |
| 3        | Dificuldades na comunicação entre os membros do grupo        | Dificuldade na execução das tarefas                                                                   | 3             | 3       | 9             | Gerenciar ativamente a comunicação com os membros                        | Tentar contatar o membro em questão por outros meios de comunicação                       |
| 4        | Requisitos mal formulados                                   | Dificuldade de compreensão dos requisitos e execução inadequada                                       | 1             | 4       | 4             | Garantir que a equipe esteja alinhada com os requisitos                       | Refazer a atividade em questão                                                       |
| 5        | Seleção inadequada de tecnologias para o projeto            | Problemas de integração entre os componentes do projeto                                               | 1            | 4       | 4             | Realizar estudos prévios sobre as tecnologias utilizadas                              | Substituição de tecnologias escolhidas |
| 6        | Baixa qualidade do produto                                  | Produto não atende completamente à proposta inicial                                                   | 2             | 3       | 6             | Manter comunicação constante entre o professor e a equipe     | Melhorar os elementos abaixo do padrão esperado                                    |                                                  |
| 7        | Priorização inadequada das atividades                       | Dificuldade em cumprir prazos esperados                                                               | 3             | 4       | 12            | Revisar constantemente as prioridades                                       | Alocar mais pessoas para tarefas importantes                                         |
| 8        | Paralisação devido a greve dos professores                  | Alteração na data de entrega do projeto                                                               | 5             | 4       | 20            | -                                                                 | -                                                                                         |
| 9       | Problemas de saúde dos membros do grupo                     | Sobrecarga nos membros restantes                                                                      | 2             | 4       | 8             | -                                                                 | Reatribuir atividades entre os membros restantes                                   |
| 10       | Baixa produtividade dos integrantes do grupo                      |  Não cumprimento do cronograma e das entregas estabelecidas                                                                      | 2             | 4       | 8             | Motivação da equipe quanto à criação do projeto através de reuniões constantes                                                            | Reatribuir atividades entre os membros mais ativos                                   |
| 11       |        Falta de cliente real              |  Baixa qualidade do produto                                                                     | 2             | 4      | 8             |                 Identificar produtos similares                         | Reformular ou incrementar requisitos                                 |
           

Tabela 5 – Tabela do Plano de Gerenciamento de Riscos 

</center>


## Referências

>  PMI - PROJECT MANAGEMENT INSTITUTE. Guia PMBOK®: Um Guia para o Conjunto de Conhecimentos em Gerenciamento de Projetos, Sexta edição, Pennsylvania: PMI, 2017.

## Histórico de Revisão

| Versão | Data       | Descrição                 | Autores    |
| ------ | ---------- | ------------------------- | ---------- |
| 1.0    | 06/05/2024 | Criação do documento     | Kayro César |