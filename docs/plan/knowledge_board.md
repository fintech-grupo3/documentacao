# Quadro de Conhecimento



## Introdução 

O quadro de conhecimento é uma ferramenta  que tem como objetivo medir o conhecimento de cada integrante da equipe em relação às tecnologias que são de fato usadas no projeto.




![Disponibilidade](../assets/Schedules/canvas_conhec.png)

## Histórico de versão
| Data | Versão | Descrição | Autor(es) |
| ---- | ---- | ---- | ---- |
| 17/04/2024 | 1.0 | Criação do Documento | Kayro César| 