# Dia 2

## Sobre

O segundo dia da Lean Inception desempenha um papel crucial no processo de definição e planejamento ágil de um projeto ou produto. Durante o Dia 2, a equipe concentra-se em duas atividades-chave: a "Descrição das Personas" e a "Jornada do Usuário". Essas atividades têm como objetivo criar uma compreensão mais profunda dos usuários e de como eles interagem com o produto.

## Descrição das Personas

Nesta etapa, a equipe começa a criar descrições detalhadas das personas que representam os diferentes tipos de usuários do produto. As personas são representações fictícias de grupos demográficos ou perfis de usuários reais que podem interagir com o produto. Cada persona recebe um nome, uma foto e uma descrição que inclui informações como idade, gênero, necessidades, desafios, metas e comportamentos típicos. O objetivo é humanizar e tornar concretas as diferentes audiências do produto, para que a equipe possa entender melhor suas necessidades e expectativas.

### Personas

#### Persona 1

![Persona 1](../assets/LeanInception/Dia2/Personas/Persona1.png)


#### Persona 2

![Persona 2](../assets/LeanInception/Dia2/Personas/Persona2.png)


#### Persona 3

![Persona 3](../assets/LeanInception/Dia2/Personas/Persona3.png)



## Jornadas do Usuário

A Jornada do Usuário envolve o mapeamento das etapas que uma persona percorre ao interagir com o produto, desde a primeira interação até a conclusão de suas tarefas ou objetivos. Isso ajuda a equipe a identificar os pontos de contato do usuário com o produto, suas emoções, necessidades e potenciais pontos problemáticos ao longo dessa jornada. O mapeamento da jornada do usuário ajuda a equipe a ter uma visão completa da experiência do usuário e a identificar oportunidades de melhoria.

### Jornadas do Usuário

#### Jornada do Usuário 1

![Jornada do Usuário 1](../assets/LeanInception/Dia2/Jornadas/Jornada1.png)

#### Jornada do Usuário 2

![Jornada do Usuário 2](../assets/LeanInception/Dia2/Jornadas/Jornada2.png)

#### Jornada do Usuário 3

![Jornada do Usuário 3](../assets/LeanInception/Dia2/Jornadas/Jornada3.png)

