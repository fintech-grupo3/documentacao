# Dia 4

## Introdução

O quarto dia do processo de Lean Inception marca um ponto crucial no desenvolvimento de um novo projeto ou produto. Durante esta fase, os conceitos e ideias previamente discutidos começam a ser solidificados em elementos tangíveis e estruturados. Dois elementos fundamentais neste estágio são o Sequenciador e o Canvas MVP, os quais desempenham papéis essenciais na definição do escopo e na estruturação do projeto.

## Sequenciador

O Sequenciador representa uma atividade na qual a equipe procede à criação de uma ordem de priorização para os recursos e funcionalidades que comporão o produto ou projeto em desenvolvimento. Nesta fase, as ideias geradas anteriormente são revisadas e organizadas em uma sequência lógica. Esta ação inclui a identificação das funcionalidades que são consideradas essenciais para o Produto Mínimo Viável (MVP), bem como aquelas que podem ser adiadas para futuras iterações. O objetivo final é estabelecer uma lista de tarefas priorizadas, orientando assim o desenvolvimento do produto.

É imprescindível observar que esta etapa é conduzida com base nos quadros das funcionalidades previamente priorizadas na fase de revisão técnica. Estas funcionalidades são alocadas em "ondas", cuja finalidade é determinar a ordem de execução das tarefas. Para assegurar a entrega de valor tanto para o cliente quanto para o usuário final, são estabelecidas algumas regras:

- **Regra 1:** Cada onda pode conter, no máximo, três cartões.
- **Regra 2:** Uma onda não pode incluir mais de um cartão vermelho.
- **Regra 3:** Uma onda não pode ser composta exclusivamente por cartões amarelos ou vermelhos.
- **Regra 4:** A soma do esforço necessário para a conclusão dos cartões em uma onda não pode exceder cinco Es.
- **Regra 5:** A soma do valor agregado pelos cartões em uma onda não pode ser inferior a quatro $s e quatro corações.
- **Regra 6:** Se um cartão depende de outro, este último deve ser incluído em uma onda anterior.

### Sequenciador do GammaBudget

![Sequenciador](../assets/LeanInception/Dia4/sequenciador.png)

## Canvas MVP

O Canvas MVP é uma ferramenta visual empregada para definir os elementos que comporão o Produto Mínimo Viável (MVP). Ele é composto por nove áreas ou blocos, sendo eles:

- **Objetivo do MVP:** Uma declaração clara dos objetivos a serem alcançados com o MVP.
- **Problema:** Uma descrição detalhada do problema que o MVP se propõe a resolver para os usuários.
- **Solução:** Uma visão geral da solução que será implementada.
- **Usuários:** Identificação dos usuários-alvo do MVP.
- **Características Principais:** Enumeração das funcionalidades essenciais que serão incluídas no MVP.
- **Métricas de Sucesso:** Definição dos indicadores que serão utilizados para mensurar o sucesso do MVP.
- **Hipóteses:** Suposições ou previsões que serão testadas por meio do MVP.
- **Restrições:** Limitações ou restrições que podem impactar o desenvolvimento do MVP.
- **Aprendizados:** Espaço reservado para registrar os aprendizados adquiridos ao longo do processo de elaboração do Canvas MVP.

O Canvas MVP oferece uma representação clara e concisa do escopo do Produto Mínimo Viável, garantindo assim que todos os membros da equipe compartilhem uma compreensão comum do que será desenvolvido e dos motivos pelos quais será desenvolvido.

![Canvas MVP](../assets/LeanInception/Dia4/canvas.png)
