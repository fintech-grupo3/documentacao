# Lean Inception

## Introdução:
&emsp;&emsp; Lean Inception é a combinação eficaz do Design thinking e do Lean StartUp para decidir o Produto Mínimo Viável (MVP). Sendo um workshop dividido em várias partes e atividades para auxiliar no entendimento e na construção do produto.

&emsp;&emsp; A princípio fizemos as seguintes dinâmicas no nosso Lean Inception:

- Visão do Produto;
- Produto É, NÁO É, FAZ, NÃO FAZ;
- Objetivos do produto;
- Personas;
- Jornada do Usuário
- Brainstorming de funcionalidades;
- Revisão técnica, de negócio e de UX
- Sequenciador;
- Canvas MVP


## Exemplo de cronograma do Lean Inception

![Cronograma Lean Inception](../assets/LeanInception/exemplo_lean.jpg)


## Lean Inception Carteira Digital

Abaixo podemos observar o resultado do Lean Inception de modo interativo, onde temos uma 
visualização completa dos passos executados para concluir o método:

[Canvas completo aqui](https://app.mural.co/t/grupo23eps0485/m/grupo23eps0485/1713303518579/21d51154d7b733b9792a87057145d1a84520e468?sender=u42b20acf0410f7c40bdf7013)

## Bibliografia

> CAROLI, P. Lean Inception. [s.l.] Editora Caroli, 2019.

## Histórico de versão
| Data | Versão | Descrição | Autor(es) |
| ---- | ---- | ---- | ---- |
| 15/04/2024 | 1.0 | Criação do Documento | Paulo Gonçalves |
