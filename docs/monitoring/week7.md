#  Sprint 3

## 1. Visão Geral


- Data de Início: 15/05/2024;
- Data de Término: 22/05/2024;
- Duração: 7 dias;

## 2. Objetivos

<p align="justify"> A Sprint 3 tem como objetivo o desenvolvimento de algumas telas do front-end e estruturação de serviços do back-end, e CI/CD do back-end.  </p>



##  3. Tarefas da Semana

| Tarefa                           | Responsáveis                          |
|----------------------------------|---------------------------------------|
| CRUD de receitas (back-end) | Lude Ribeiro                        |
| Tela de Login | João Vitor            |
| Tela de Cadastro de usuário | João Vitor   e Kayro César          |
| Tela Inicial | Kayro César             |
| CI/CD do back-end | Paulo Gonçalves                      |





