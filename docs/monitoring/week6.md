#  Sprint 2

## 1. Visão Geral


- Data de Início: 08/05/2024;
- Data de Término: 15/05/2024;
- Duração: 7 dias;

## 2. Objetivos
A Sprint 2 tem como objetivo construir alguns artefatos referentes à documentação.


##  3. Tarefas da Semana

| Tarefa                           | Responsáveis                          |
|----------------------------------|---------------------------------------|
| Plano de custo | Lude Ribeiro                        |
| Plano de Riscos | Kayro César           |
| Plano de comunicação | Kayro César           |
| Plano de qualidade | Paulo Gonçalves                      |
| Protótipo (Figma)                | Kayro César e João Vitor               |
| Documento de Arquitetura         | Kayro César, João Vitor, Lude Ribeiro |
| Guia de Commits | Paulo Gonçalves                      |


- *Obs:  Por solicitação do professor, no dia 10/05/2024 foi gravado um vídeo para apresentar o protótipo do projeto feito no Figma, bem como a estrutura arquitetural e os serviços que o software iria possuir. O vídeo foi gravado pelos integrantes Kayro César e Lude Ribeiro, na plataforma Microsoft Teams e enviado no grupo da disciplina.





