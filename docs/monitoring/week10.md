#  Sprint 6

## 1. Visão Geral


- Data de Início: 05/06/2024;
- Data de Término: 12/06/2024;
- Duração: 7 dias; 

## 2. Objetivos
A Sprint 6 tem como objetivo fazer a correção de erros identificados durante a integração, discussão de serviços a serem dispobilizados para consumo de outros grupos, bem como da escolha de quais serviços de outros grupos podem encaixar com a temática da nossa aplicação. Além disso, será realizada a construção de mais telas no front-end e de mais serviços no back-end, bem como da implementação do ci/cd do front-end.


##  3. Tarefas da Semana

| Tarefa                           | Responsáveis                          |
|----------------------------------|---------------------------------------|
| Cadastro de alertas (back-end) | Lude Ribeiro                        |
| Tela de alertas (front-end) |    Kayro César           |
| Tela de relatórios (front-end) |  João Vitor 
| CI/CD (front-end)| Paulo Gonçalves      
| Discussão acerca dos serviços a serem disponibilizados e consumidos | Todos      





* Obs: Nessa sprint também foi realizada a gravação do pitch do produto para o desafio Sebrae, o vídeo foi enviado no grupo da disciplina (todos participaram).