#  Sprint 7

## 1. Visão Geral


- Data de Início: 12/06/2024;
- Data de Término: 19/06/2024;
- Duração: 7 dias; 

## 2. Objetivos
A Sprint 7 tem como objetivo dar continuidade no desenvolvimento das telas do front-end e nos serviços do back-end. Além disso, devem ser definidos e iniciadas as implementações visando adaptar integrar os serviços a serem consumidos e os serviços a serem disponibilizados


##  3. Tarefas da Semana

| Tarefa                           | Responsáveis                          |
|----------------------------------|---------------------------------------|
| Agendamento de alertas por email (back-end) | Lude Ribeiro                        |
| Tela de limites (front-end) |    Kayro César           |
| Continuação da Tela de relatórios (front-end)  |  João Vitor 
| Continuação CI/CD (front-end)| Paulo Gonçalves      
| Definição dos serviços a serem disponibilizados e consumidos | Todos      






