#  Sprint 10

## 1. Visão Geral


- Data de Início: 03/07/2024;
- Data de Término: 09/07/2024;
- Duração: 6 dias; 

## 2. Objetivos
A Sprint 10 tem como objetivo finalizar tarefas pendentes, realizar correções de erros, revisar o projeto como um todo, escrever testes, realizar a documentações pendentes e realizar uma autoavaliação.

##  3. Tarefas da Semana

| Tarefa                           | Responsáveis                          |
|----------------------------------|---------------------------------------|
| Correções gerais (back-end) | Lude Ribeiro                        |
| Correção gerais (front-end) |  João Vitor  e Kayro César
| Correções gerais ci/cd (front e back)| Paulo Gonçalves      
| Autoavaliação | Todos 







