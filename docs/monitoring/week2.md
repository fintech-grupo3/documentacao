# Semana 2 (10/04 a 17/04)


## **Discussão**



<p align="justify"> Um vez escolhido o tema, o grupo destinou a semana na discussão e elaboração des artefatos presentes no quadro do Lean Inception. Começamos definindo o objetivo do produto, esclarecendo seu propósito e metas principais. Em seguida, identificamos e descrevemos  as personas, considerando as necessidades e expectativas dos usuários. Mapeamos a jornada do usuário para entender melhor as etapas que eles percorrem ao interagir com o produto. Também desenvolvemos a visão do produto, destacando suas funcionalidades e características principais. Além disso, organizamos as funcionalidades em uma sequência através do sequenciador do Lean e definimos um plano de releases para entregas incrementais, priorizando as funcionalidades de maior valor. Para a semana seguinte restará apenas a contrução do quadro do Canvas MVP.
</p>



## **Objetivos**

- Elaborar artefatos do Lean Inception para o projeto do grupo


## **Ações futuras**

- Contruir o  Quadro do Canvas MVP

