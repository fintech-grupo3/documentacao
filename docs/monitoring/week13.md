#  Sprint 9

## 1. Visão Geral


- Data de Início: 26/06/2024;
- Data de Término: 03/07/2024;
- Duração: 7 dias; 

## 2. Objetivos
A Sprint 9 tem como objetivo integrar as novas telas e serviços desenvolvidos após a primeira parte do MVP e documentar as apis de serviços presentes no back-end. Além disso, devem ser finalizados o consumo e a disponibilização de serviços para outros grupos e a elaboração de slides para apresentação do produto em sala de aula.

##  3. Tarefas da Semana

| Tarefa                           | Responsáveis                          |
|----------------------------------|---------------------------------------|
| Disponibilização de serviço de cadastro de despesa (back-end) | Lude Ribeiro                        |
| Disponibilização de serviço de geração de termos de privacidade (back-end) | Lude Ribeiro                        |
| Consumo de serviço de geração de termos de uso (front-end)|    Kayro César           |
| Correção de erros e ajustes no front-end   |  João Vitor 
| Documentação de apis do back-end com swagger| Paulo Gonçalves
| Elaboração  de  slides| Todos
     


<br>
<br>


| **Serviços Disponibilizados para Outros Grupos** | **Grupo**       |
|--------------------------------------------------|-----------------|
| Geração de PDF de termos de privacidade          | DataMed         |
| Cadastro de despesas, suas informações e categorias | SwiftPix      |

| **Serviços Consumidos de Outros Grupos**         | **Grupo**       |
|--------------------------------------------------|-----------------|
| Criptografia de informações                      | CryptoBot       |
| Geração de termos de uso                         | DataMed         |



