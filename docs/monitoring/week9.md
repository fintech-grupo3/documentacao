#  Sprint 5

## 1. Visão Geral


- Data de Início: 29/05/2024;
- Data de Término: 02/06/2024;
- Duração: 4 dias; (Reduzida devido a entrega do MVP-1)

## 2. Objetivos
A Sprint 5 tem como objetivo fazer a correção de erros e a integração entre as primeiras telas contruidas do front-end com os serviços contruidos no back-end, além disso deve ser realizado o deploy tanto do back-end quanto do front-end, de forma que os dois se comuniquem já no ar. O fim dessa Sprint coincide com a entrega da primeira parte do MVP.


##  3. Tarefas da Semana

| Tarefa                           | Responsáveis                          |
|----------------------------------|---------------------------------------|
| Correção de Erros em serviços do back-end | Lude Ribeiro                        |
| Correção de Erros em serviços do front-end | João Vitor  e Kayro César           |
| Deploy do front-end | Kayro César, João Vitor e Lude Ribeiro               |
| Deploy do back-end | Paulo Gonçalves       e Lude Ribeiro               |
| Continuação de testes para o back-end | Paulo Gonçalves    e Lude Ribeiro                  |
| Integração entre front-end e back-end | Todos      |



