# Semana 3 (17/04 a 24/04)



## **Discussão**



<p align="justify">Durante a semana, nos concentramos na correção e revisão dos artefatos do Lean Inception e na contrução do quadro do Canvas MVP. Este trabalho envolveu ajustar o propósito do produto,suas metas, refinar a descrição  das personas, revisar o mapeamento da jornada do usuário e aprimorar a visão do produto. Também organizamos as funcionalidades em uma sequência lógica de desenvolvimento e revisamos o plano de releases para entregas, priorizando as funcionalidades de maior valor. As ações futuras incluem a construção de um cronograma e o início da configuração dos ambientes de desenvolvimento.  </p>


## **Objetivos**

- Construir o Quadro do Canvas MVP
- Corrigir artefatos do Lean Inception 



## **Ações futuras**

- Construir cronograma 
- Iniciar Configurar Ambientes de desenvolvimento 
