# Sobre o GammaBudget

<div style="text-align: center;">
    <img src="./assets/logo/green-logo.png" alt="logo" width="400" height="400">
</div>

O GammaBudget é uma aplicação web que ajuda os usuários a administrar suas finanças diárias, fornecendo uma visão clara de seus gastos, receitas e saldos. Ele simplifica o processo de acompanhamento de finanças, permitindo que os usuários organizem e monitorem suas transações financeiras.

## Equipe

A equipe é composta por 4 membros ao todo, afim de documentar e desenvolver o projeto.

### Integrantes

<div style="display: flex; flex-wrap: wrap; justify-content: center; gap: 2em; max-width: 800px;">
  <div style="flex: 1 1 calc(50% - 1em); display: flex; flex-direction: column; align-items: center; margin-bottom: 2em;">
    <div style="width: 180px; height: 180px;">
      <img style="border-radius: 50%; border: 2px solid #008080;" src="https://gitlab.com/fintech-grupo3/documentacao/-/raw/main/docs/assets/team/joao.png?ref_type=heads"/>
    </div>
    <label>João Vitor Ferreira Alves</label>
  </div>

  <div style="flex: 1 1 calc(50% - 1em); display: flex; flex-direction: column; align-items: center; margin-bottom: 2em;">
    <div style="width: 180px; height: 180px;">
      <img style="border-radius: 50%; border: 2px solid #008080;" src="https://gitlab.com/fintech-grupo3/documentacao/-/raw/main/docs/assets/team/kayro.jpg?ref_type=heads"/>
    </div>
    <label>Kayro César Silva Machado</label>
  </div>

  <div style="flex: 1 1 calc(50% - 1em); display: flex; flex-direction: column; align-items: center; margin-bottom: 2em;">
   <div style="width: 180px; height: 180px;">
      <img style="border-radius: 50%; border: 2px solid #008080;" src="https://gitlab.com/fintech-grupo3/documentacao/-/raw/main/docs/assets/team/lude.jpeg?ref_type=heads"/>
    </div>
    <label>Lude Yuri de Castro Ribeiro</label>
  </div>

  <div style="flex: 1 1 calc(50% - 1em); display: flex; flex-direction: column; align-items: center; margin-bottom: 2em;">
    <div style="width: 180px; height: 180px;">
      <img style="border-radius: 50%; border: 2px solid #008080;" src="https://gitlab.com/fintech-grupo3/documentacao/-/raw/main/docs/assets/team/paulo.png?ref_type=heads"/>
    </div>
    <label>Paulo Gonçalves Lima</label>
  </div>
</div>

</div>
