## Backlog

### Planejado



| **ID** | **Item** | **Descrição** |
|--------|----------|---------------|
| 1 | CRUD de Usuário | Implementar funcionalidade para criar, ler, atualizar e deletar usuários no sistema. |
| 2 | CRUD de Receitas | Implementar funcionalidade para criar, ler, atualizar e deletar receitas no sistema. |
| 3 | CRUD de Despesas | Implementar funcionalidade para criar, ler, atualizar e deletar despesas no sistema. |
| 4 | Categorização de Receitas | Implementar a categorização das receitas para facilitar a organização e análise. |
| 5 | Categorização de Despesas | Implementar a categorização das despesas para facilitar a organização e análise. |
| 6 | Cálculo de Saldo Mensal | Implementar o cálculo automático do saldo mensal com base nas receitas e despesas. |
| 7 | Definição de Limite de Gastos | Permitir aos usuários definir limites de gastos para controlar as despesas. |
| 8 | Criação das Notificações de Contas a Pagar | Implementar sistema de notificações para alertar os usuários sobre contas a pagar. |
| 9 | Relatório de Despesas | Implementar relatórios  sobre as despesas dos usuários. |
| 10 | Relatório de Receitas | Implementar relatório sobre as receitas dos usuários. |
| 11 | Relatório de Saldo | Implementar relatórios detalhados sobre o saldo financeiro dos usuários. |
| 11 | Relatório Geral | Implementar relatórios  sobre o saldo financeiro dos usuários. |






### Incremento

| **ID** | **Item** | **Descrição** |
|--------|----------|---------------|
| 12 | Relatório com Análise e Recomendação | Implementar relatórios que forneçam análises detalhadas e recomendações financeiras baseadas nos dados de despesas e receitas. |
| 13 | Integração com Open Finance | Implementar integração com o sistema Open Finance para acessar dados financeiros de diferentes fontes. |

