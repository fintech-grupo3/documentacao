# Portótipo

## Introdução


Na área de desenvolvimento de software, os protótipos desempenham um papel crucial na concepção e no aprimoramento de sistemas e aplicativos. Um protótipo é uma versão inicial de um software que permite aos desenvolvedores, designers e partes interessadas visualizarem e interagirem com as funcionalidades e a aparência do sistema antes da implementação final. Essas representações são fundamentais para validar conceitos, coletar feedback, identificar requisitos e economizar tempo e recursos durante o ciclo de desenvolvimento.

Os protótipos podem ser classificados em duas categorias distintas: protótipos de baixa fidelidade e protótipos de alta fidelidade. A diferença principal entre esses tipos de protótipos está na profundidade do detalhe e na precisão com que representam o sistema final. Protótipos de baixa fidelidade são simplificados e rápidos de criar, ideais para as fases iniciais de exploração de ideias e conceitos. Já os protótipos de alta fidelidade se aproximam mais do produto final em termos de funcionalidade e aparência, sendo úteis para validar detalhes específicos e refinamentos mais avançados do design.

## Protótipo GammaBudget

A prototipação que ocorreu no projeto GammaBudget  foi dessa desenvolvida com base nas idéias que levantamos durante a criação da visão do produto. Entre as telas protótipadas temos: tela de login, tela de cadastro, tela inicial, tela de lançamentos, tela de limite de gastos e tela de alertas.

## Protótipo Funcional

Para o GammaBudget, desenvolvemos o seguinte protótipo descrito a seguir, ressalta-se que alguns detalhes de elementos presentes no protótipo ainda podem sofrer mudanças a depender de mudanças de escopo e de requisitos.

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fdesign%2FXQoEU9eUD0IzzkesuGxhDn%2Ffinacas%3Fnode-id%3D0%253A1%26t%3DOWM6GopxIemfhVjG-1" allowfullscreen></iframe>

