# Documento de Arquitetura

## Introdução

Este documento visa esclarecer as decisões arquiteturais do projeto, concentrando-se na compreensão do funcionamento do sistema em sua totalidade. Isso inclui a interação do usuário com o produto, a definição dos microsserviços e outras questões relevantes que possam surgir.

## Escopo


O GammaBudget é um aplicativo desenvolvido para  usuários na organização e acompanhamento das finanças pessoais. Este acompanhamento pode ocorrer com acompanhamento de despesas e receitas, ambos podendo ser categorizados e separados. Existem possibilidades de definir limites de gastos, bem como ativação de alertas e geração de relatórios personalizados com diversos filtros.



## Arquitetura

A arquitetura abaixo ilustra a forma como irá funcionar todas as partes, sendo elas o Frontend, onde através da interface de usuário, as informações são disponibilizadas pelo usuário e  encaminhadas para o microsserviço especificado, sendo eles o Microsserviço Usuário atrelado as funcionalidades de CRUD , Microsserviço  despesas e receitas e  o  Microsserviço de relatório de análise financeira. Além disso temos os bancos de cada microsserviço, responsável por armazenar todas essas informações. O diagrama abaixo apresenta esses elementos:




<div style="text-align: center;">
    <img src="https://gitlab.com/fga-eps-rmc/fintech/grupo3/fintech-grupo3/documentacao/-/raw/main/docs/assets/architecture/diag-arq.png?ref_type=heads" alt="diagrama de arquitetura">
</div>




## Microsserviço Usuário

O Microsserviço de Usuário no aplicativo GammaBudget desempenha um papel essencial na autenticação  de usuários. Este microsserviço oferece funcionalidades fundamentais relacionadas ao registro e gerenciamento de contas de usuário, permitindo que os usuários acessem as diversas funcionalidades do sistema. Suas principais funcionalidades incluem:

1. **CRUD de Usuário (Create, Read, Update, Delete):** Os usuários têm a capacidade de se registrar no aplicativo, fornecendo informações pessoais, como nome, endereço de e-mail, senha  e outros detalhes relevantes. Esse processo de registro é fundamental para criar uma conta no aplicativo, permitindo o acesso às funcionalidades e recursos disponíveis.

Além disso, o microsserviço de Usuário também se concentra em questões de segurança, fornecendo uma funcionalidade de recuperação de senha para garantir a proteção dos dados dos usuários. Isso ajuda a garantir que apenas usuários autorizados tenham acesso ao sistema e que suas informações estejam protegidas.



## Microsserviço despesas e receitas

O microsserviço de despesas e receitas no aplicativo GammaBudget  é projetado para fornecer funcionalidades relacionadas ao compartilhamento de informações, discussões e interações no fórum do aplicativo. Suas principais funcionalidades incluem:

O microsserviço de despesas e custos no aplicativo GammaBudget é projetado para oferecer aos usuários  o registro e gerenciamento eficiente de suas despesas financeiras, com foco na categorização e controle de custos. Suas principais funcionalidades incluem:

1. **CRUD de despesas**: Os usuários podem  registrar suas despesas financeiras diárias, semanais ou mensais, proporcionando uma visão detalhada de seus gastos.


2. **CRUD de receitas**: Os usuários podem  registrar suas receitas financeiras diárias, semanais ou mensais, proporcionando uma visão detalhada de seus gastos.

3. **Categorização**: Os usuários podem categorizar suas despesas e receitas em diferentes áreas, como alimentação, moradia, transporte, lazer, entre outros, facilitando uma compreensão clara de onde o dinheiro está sendo direcionado.

4. **Criação de Alertas :** Os usuários podem receber notificações do aplicativo para lembrá-los de realizar pagamentos de despesas cadastradas ou para alertar sobre o limite de gastos estabelecido.


5. **Estabelecimento de Limite de Gastos:** Os usuários têm a capacidade de definir um limite de gastos para o mês. Isso permite um controle mais rigoroso das despesas e ajuda os usuários a permanecerem dentro de seus orçamentos.

## Microsserviço de relatório de análise financeira

O microsserviço de relatórios e análises financeiras no aplicativo GammaBudget é uma ferramenta  para os usuários obterem insights  sobre sua saúde financeira e tomar decisões informadas. Suas principais funcionalidades incluem:

1. **Relatórios Personalizados:** Os usuários têm acesso a um relatório personalizável, que abrange desde o panorama geral das finanças até detalhes específicos de categorias de despesas e receitas. Esses relatórios fornecem uma visão da situação financeira de cada usuário.




## Histórico de versão

| Versão | Data       | Descrição | Autores |
| ------ | ---------- | --------- | ------- |
| 1.0    | 07/05/2024 | Criação do documento | Kayro César, João Vítor e Paulo Gonçalves|
