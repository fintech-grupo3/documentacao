# Guia de Estilo

## 1. Introdução

Este documento  apresenta as diretrizes de design que moldam a apresentação visual e interativa de nossa aplicação.

## 2. Nome da aplicação


A escolha do nome "GammaBudget" foi  considerada "Gamma" para fazer referência ao campus em que estudamos. "Budget" significa orçamento em inglês, indicando  o foco do aplicativo: o controle financeiro pessoal. Juntos, essas palavras comunicam nosso objetivo em fornecer uma ferramenta de gerenciamento financeiro pessoal.


## 3. Logo


Além da logo principal, desenvolvemos outras duas logos: Uma de cor  verde e outra de cor branca. Tais logos visam uma melhor combinação com as cores de fundo estabelecidas dentro de cada uma das tela do GammaBudget . Aquelas com o fundo branco ou claro utilizam a logo na cor verde (para dar contraste), enquanto que em telas com cor de fundo mais escuras a logo branca é utilizada. 

<div style="text-align: center;">
    <img src="https://gitlab.com/fga-eps-rmc/fintech/grupo3/fintech-grupo3/documentacao/-/raw/main/docs/assets/logo/logo.png?ref_type=heads" alt="logo" width="250" height="250">
</div>

<div style="text-align: center;">
    <img src="https://gitlab.com/fga-eps-rmc/fintech/grupo3/fintech-grupo3/documentacao/-/raw/main/docs/assets/logo/green-logo.png?ref_type=heads" alt="logo" width="250" height="250">
     <img src="https://gitlab.com/fga-eps-rmc/fintech/grupo3/fintech-grupo3/documentacao/-/raw/main/docs/assets/logo/w-logo.png?ref_type=heads" alt="logo" width="250" height="250">

</div>


# 4. Tipografia

As fontes utilizadas na aplicação serão:

- **Roboto**
![Roboto](../assets/fonts/roboto.png)
- **Open Sans**
![Open Sans](../assets/fonts/opensans.png)
- **Montserrat**
![Montserrat](../assets/fonts/montserrat.png)

# 5. Paleta de cores

![paleta de cores](../assets/pallete/colors.png)

## 6. Elementos de interação

### 6.1. Estilos de interação

* Navbar: será uma barra horizontal para a navegação do usuário no aplicativo. Cada item da navbar redirecionará a navegação para uma tela específica.

* Vertical Scroll Picker: componente de interface do usuário que permite aos usuários fazerem seleções através de uma rolagem vertical. Esse tipo de controle é frequentemente usado para escolher entre diferentes opções, valores ou itens em uma lista, percorrendo-os para cima ou para baixo.

* Botão: botões clicáveis com imagens que, ao ser clicado, leva o usuário até o conteúdo requerido

* Botões de alternância (toggle): elementos de interface que permitem aos usuários ativar ou desativar uma opção com um simples toque ou clique. 

* Checkbox: recurso comum às caixas de diálogos usado para ativar ou desativar mais de uma função em um programa. Visualmente é representado por um quadrado branco, que quando selecionado apresenta uma marcação em seu interior.

* Carousel: um slideshow para fazer um giro em vários coteúdos, construído com CSS 3D transforms e um pouco de JavaScript. Ele funciona com o uso de imagens, texto ou marcação personalizada. Também possui suporte para controles anterior, próximo e indicadores.

* Campo de pesquisa: campo utilizado pelo usuário para fazer pesquisa rápida de algum conteúdo, dentro do aplicativo.


* Cards: aqui serão os elementos que possuem geralmente uma imagem, pois podemos adicionar à essa imagem uma descrição. Ele pode ser tanto um link como um card explicativo, que poderá expandir para mostrar um conteúdo maior.


* Sliders: é um painel de conteúdos em que o seu conteúdo principal muda em um tempo determinado para dar lugar a outro conteúdo. Isso serve para mostrar conteúdos variados em um mesmo lugar para o usuário. O mesmo poderá navegar entre os conteúdos ou clicar para ser redirecionado à página deste conteúdo.

* Formulários: serão utilizados para login, para contato ou para qualquer outra atividade em que é necessário obter uma informação do usuário.
